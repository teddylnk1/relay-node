# cython: embedsignature=True, binding=True
import logging

from ipv8.loader import IPv8CommunityLoader
from ipv8.taskmanager import TaskManager

from ipv8_service import IPv8

from loadmodules import register_default_launchers

class Session(TaskManager):

    __single = None

    def __init__(self, core_test_mode=False,mode=None):
        """
        A Session object is created
        Only a single session instance can exist at a time in a process.
        """
        super().__init__()

        

        self._logger = logging.getLogger(self.__class__.__name__)

        #self.config = config
        self.ipv8 = None
        self.ipv8_start_time = 0

        self._logger = logging.getLogger(self.__class__.__name__)

        self.bootstrap = None

        # modules
       
        self.ipv8_community_loader = IPv8CommunityLoader()
        if mode != None:  
            register_default_launchers(self.ipv8_community_loader, mode)

        self.tunnel_community = None
        self.dht_community = None
        self.mds = None  # Metadata Store

        # In test mode, the Core does not communicate with the external world and the state dir is read-only
        self.core_test_mode = core_test_mode

    def load_ipv8_overlays(self):
        self.ipv8_community_loader.load(self.ipv8, self)

    async def start(self,address):
        """
        Start session by initializing the LaunchManyCore class
        """
        from ipv8.configuration import ConfigBuilder
        from ipv8 import configuration
        from ipv8.messaging.interfaces.dispatcher.endpoint import DispatcherEndpoint
        configuration.DISPERSY_BOOTSTRAPPER = {
            'class': "DispersyBootstrapper",
            'init': {
                'ip_addresses': [
                    ("130.161.119.206", 6421),
                    ("130.161.119.206", 6422),
                    ("131.180.27.155", 6423),
                    ("131.180.27.156", 6424),
                    ("131.180.27.161", 6427),
                    ("131.180.27.161", 6521),
                    ("131.180.27.161", 6522),
                    ("131.180.27.162", 6523),
                    ("131.180.27.162", 6524),
                    ("130.161.119.215", 6525),
                    ("130.161.119.215", 6526),
                    ("130.161.119.201", 6527),
                    ("130.161.119.201", 6528),
                    ("95.217.88.83", 8090)
                ],
                'dns_addresses': [
                    (u"dispersy1.tribler.org", 6421), (u"dispersy1.st.tudelft.nl", 6421),
                    (u"dispersy2.tribler.org", 6422), (u"dispersy2.st.tudelft.nl", 6422),
                    (u"dispersy3.tribler.org", 6423), (u"dispersy3.st.tudelft.nl", 6423),
                    (u"dispersy4.tribler.org", 6424),
                    (u"tracker1.ip-v8.org", 6521),
                    (u"tracker2.ip-v8.org", 6522),
                    (u"tracker3.ip-v8.org", 6523),
                    (u"tracker4.ip-v8.org", 6524),
                    (u"tracker5.ip-v8.org", 6525),
                    (u"tracker6.ip-v8.org", 6526),
                    (u"tracker7.ip-v8.org", 6527),
                    (u"tracker8.ip-v8.org", 6528)
                ],
                'bootstrap_timeout': 30.0
            }
        }
        bootstrap_config =  configuration.DISPERSY_BOOTSTRAPPER['init']
        bootstrap_config['ip_addresses'].append(address)
        ipv8_config_builder = (ConfigBuilder()
                                .set_port(8090)
                                .set_address('0.0.0.0')
                                .clear_overlays()
                                .clear_keys()  # We load the keys ourselves
                                .set_working_directory('.')
                                .set_walker_interval(0.5))
      

        endpoint = DispatcherEndpoint(["UDPIPv4"], UDPIPv4={'port': 8090,
                                                   'ip': '0.0.0.0'})
 
        self.ipv8 = IPv8(ipv8_config_builder.finalize(), endpoint_override=endpoint)
      
        await self.ipv8.start()

        
        self.load_ipv8_overlays()

    async def shutdown(self):
        if self.ipv8:
            await self.ipv8.stop(stop_loop=False)
        self.ipv8 = None