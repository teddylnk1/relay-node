import sys
import grpc
import pickle
import os 

sys.path.append("./service_spec")
import service_pb2
import service_pb2_grpc


from ipv8.peer import Peer
from ipv8.keyvault.public.libnaclkey import LibNaCLPK

class Session:
    def __init__(self):
        self.master = None
        self.slave = None



def encode_message(data):
    return pickle.dumps(data)


def decode_message(payload):
    return pickle.loads(payload)

def get_endpoints(service_name):
    with grpc.insecure_channel(str(registry_address)) as channel:
        stub = service_pb2_grpc.RegistryStub(channel)
        response = stub.reqServiceEndpoints(
            service_pb2.ServiceRequest(service_name=service_name))
        endpoints = pickle.loads(response.endpoints)

    return endpoints
