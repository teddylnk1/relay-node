import os
from asyncio import ensure_future, get_event_loop

import libnacl

from ipv8.community import Community, DEFAULT_MAX_PEERS
from ipv8.configuration import ConfigBuilder, Strategy, WalkerDefinition, default_bootstrap_defs
from ipv8_service import IPv8
from ipv8.lazy_community import lazy_wrapper
import ipv8_messages
import time
import pickle
from utils import Session


class LinkEncryptedCommunity(Community):

    community_id = b'\xc2\x98da\x0b\xa7LN\x8a(\xc1\xfcV\xf7m0\xc5s\xc8\x21'

    def __init__(self, my_peer, endpoint, network, max_peers=DEFAULT_MAX_PEERS, anonymize=False):
        super().__init__(my_peer, endpoint, network, max_peers, anonymize)
        self.known_peers = {}
        self.sessions = {}
        self.master = None
        self.slave = None
        # self.start()
        # self.once = False
        self.add_message_handler(ipv8_messages.PingRequest, self.on_ping_request)
        self.add_message_handler(ipv8_messages.QueryRelay, self.on_query_relay)
        self.add_message_handler(ipv8_messages.InitiateConnection, self.on_initiate_connection)
        self.add_message_handler(ipv8_messages.ConnectionAccepted, self.on_connection_accepted)
        self.add_message_handler(ipv8_messages.DataPayload, self.on_datapayload)

    @lazy_wrapper(ipv8_messages.PingRequest)
    async def on_ping_request(self, peer, payload):
        print("Received ping request from ", peer)
        print("Known peers: ", [peers._address for peers in self.known_peers.keys()])
        print("Known peers: ", [peers.mid for peers in self.known_peers.keys()])
        update_time = time.time()
        self.known_peers[peer] = update_time
        self.ez_send(peer, ipv8_messages.PingReply(str.encode("Registered")))

    @lazy_wrapper(ipv8_messages.QueryRelay)
    async def on_query_relay(self, peer, payload):
        return_peer = peer
        print("*********************************")
        print("Received query relay from ", peer)
        request = pickle.loads(payload.content)
        for peer in self.known_peers.keys():
            print("Requested mid",request['node']['mid'])
            print("Peer mid", peer.mid)
            if request['node']['mid'] == peer.mid:
                print("Found matching peer")
                print("Reply sent to ", peer._address)
                self.ez_send(return_peer, ipv8_messages.QueryReply(str.encode('yes')))
        print("*********************************")

    @lazy_wrapper(ipv8_messages.InitiateConnection)
    async def on_initiate_connection(self, peer, payload):
        """[When requested to inititate connection the relay node
            stores the initiator as master and requests connection to the destination]

        Args:
            peer ([type]): [description]
            payload ([type]): [description]
        """           
        print("Received initiate connection from ", peer)    
        message = pickle.loads(payload.content)
        uid = message['uid']
        node = message['node']
        session = Session()
        session.master = peer
        for peer in self.known_peers.keys():
            if node['mid'] == peer.mid:
                session.slave = peer
        self.sessions[message['uid']] = session
        self.ez_send(session.slave, ipv8_messages.ConnectionRequest(pickle.dumps(message)))
        # Done until 4

    @lazy_wrapper(ipv8_messages.ConnectionAccepted)
    async def on_connection_accepted(self, peer, payload):
        print("Received connection accepted from ", peer)
        uid = pickle.loads(payload.content)
        session = self.sessions[uid]
        master = session.master
        slave = session.slave
        self.ez_send(master,ipv8_messages.SessionCreated(str.encode("Yes")))
        self.ez_send(slave,ipv8_messages.SessionCreated(str.encode("Yes")))

    @lazy_wrapper(ipv8_messages.DataPayload)
    async def on_datapayload(self, peer, payload):
        print("Received data payload from ", peer)
       
        msg = pickle.loads(payload.content)
       
        uid = msg['uid']
        session = self.sessions[uid]
        if msg['master']:
            print("command", msg['command'])
            self.ez_send(session.slave, ipv8_messages.DataPayload(pickle.dumps(msg)))
        else:
            self.ez_send(session.master, ipv8_messages.DataPayload(pickle.dumps(msg)))

    async def ping_relay(self):
        if self.get_peers():
            for peer in self.get_peers():
                self.ez_send(peer, ipv8_messages.PingRequest(
                    str.encode(time.time())))


    # def start(self):
    #     async def start_communication():
    #         self.get_peers()
    #         if (self.get_peers()):
    #             if (not self.once):
    #                 print(">", end=" ")
    #                 command = input()
    #                 self.send_example_message(
    #                     self.get_peers()[0], str.encode(command))
    #                 self.received_response = False

    #     self.register_task("start_communication",
    #                     start_communication, interval=5.0, delay=0)
