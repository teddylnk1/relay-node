# pylint: disable=import-outside-toplevel
import inspect
import sys

from ipv8.loader import CommunityLauncher,  kwargs, overlay,  set_in_session, walk_strategy
from ipv8.peer import Peer
from ipv8.keyvault.crypto import default_eccrypto
from ipv8.peerdiscovery.discovery import RandomWalk


INFINITE = -1
"""
The amount of target_peers for a walk_strategy definition to never stop.
"""


class SlaveCommunityLauncher(CommunityLauncher):
    def prepare(self, overlay_provider, session):
        """
        Perform setup tasks before the community is loaded.

        :type overlay_provider: ipv8.IPv8
        :type session: object
        """
        # print("in prepare")

    def get_overlay_class(self):
        """
        Get the overlay class this launcher wants to load.

        This raises a RuntimeError if it was not overwritten at runtime, to appease Pylint.

        :rtype: ipv8.overlay.Overlay
        """
        # print("in get_overlay_class")
        from slave import LinkEncryptedCommunity
        return LinkEncryptedCommunity

    def get_walk_strategies(self):
        """
        Get walk strategies for this class.
        It should be provided as a list of tuples with the class, kwargs and maximum number of peers.
        """
        # return [(RandomWalk,{'timeout': 3.0}, 10),(PeriodicSimilarity, {},INFINITE),(RandomChurn,{},INFINITE)]
        return [(RandomWalk,{'timeout': 3.0}, 10)]

    def get_my_peer(self, ipv8, session):
        return Peer(default_eccrypto.generate_key("curve25519"))

    def get_bootstrappers(self, session):
        from ipv8.bootstrapping.dispersy.bootstrapper import DispersyBootstrapper
        from ipv8.configuration import DISPERSY_BOOTSTRAPPER

        return [(DispersyBootstrapper, DISPERSY_BOOTSTRAPPER['init'])]


class MasterCommunityLauncher(CommunityLauncher):
    def prepare(self, overlay_provider, session):
        """
        Perform setup tasks before the community is loaded.

        :type overlay_provider: ipv8.IPv8
        :type session: object
        """
        # print("in prepare")

    def get_overlay_class(self):
        """
        Get the overlay class this launcher wants to load.

        This raises a RuntimeError if it was not overwritten at runtime, to appease Pylint.

        :rtype: ipv8.overlay.Overlay
        """
        # print("in get_overlay_class")
        from master import LinkEncryptedCommunity
        return LinkEncryptedCommunity

    def get_walk_strategies(self):
        """
        Get walk strategies for this class.
        It should be provided as a list of tuples with the class, kwargs and maximum number of peers.
        """
        # return [(RandomWalk,{'timeout': 3.0}, 10),(PeriodicSimilarity, {},INFINITE),(RandomChurn,{},INFINITE)]
        return [(RandomWalk,{'timeout': 3.0}, 10)]

    def get_my_peer(self, ipv8, session):
        return Peer(default_eccrypto.generate_key("curve25519"))

    def get_bootstrappers(self, session):
        from ipv8.bootstrapping.dispersy.bootstrapper import DispersyBootstrapper
        from ipv8.configuration import DISPERSY_BOOTSTRAPPER

        return [(DispersyBootstrapper, DISPERSY_BOOTSTRAPPER['init'])]


class RelayCommunityLauncher(CommunityLauncher):
    def prepare(self, overlay_provider, session):
        """
        Perform setup tasks before the community is loaded.

        :type overlay_provider: ipv8.IPv8
        :type session: object
        """
        # print("in prepare")

    def get_overlay_class(self):
        """
        Get the overlay class this launcher wants to load.

        This raises a RuntimeError if it was not overwritten at runtime, to appease Pylint.

        :rtype: ipv8.overlay.Overlay
        """
        # print("in get_overlay_class")
        from relay import LinkEncryptedCommunity
        return LinkEncryptedCommunity

    def get_walk_strategies(self):
        """
        Get walk strategies for this class.
        It should be provided as a list of tuples with the class, kwargs and maximum number of peers.
        """
        # return [(RandomWalk,{'timeout': 3.0}, 10),(PeriodicSimilarity, {},INFINITE),(RandomChurn,{},INFINITE)]
        return [(RandomWalk,{'timeout': 3.0}, 10)]

    def get_my_peer(self, ipv8, session):
        return Peer(default_eccrypto.generate_key("curve25519"))

    def get_bootstrappers(self, session):
        from ipv8.bootstrapping.dispersy.bootstrapper import DispersyBootstrapper
        from ipv8.configuration import DISPERSY_BOOTSTRAPPER

        return [(DispersyBootstrapper, DISPERSY_BOOTSTRAPPER['init'])]


class TestnetMixIn:
    def should_launch(self, session):
        return True


# strategies
def random_walk():
    from ipv8.peerdiscovery.discovery import RandomWalk
    return RandomWalk

# @kwargs(max_peers='100')
# @walk_strategy(random_walk)
# class MasterCommunityLauncher(IPv8CommunityLauncher):
#     pass

# @kwargs(max_peers='100')
# @walk_strategy(random_walk)
# class SlaveCommunityLauncher(IPv8CommunityLauncher):
#     pass


def get_hiddenimports():
    """
    Return the set of all hidden imports defined by all CommunityLaunchers in this file.
    """
    hiddenimports = set()

    for _, obj in inspect.getmembers(sys.modules[__name__]):
        hiddenimports.update(getattr(obj, "hiddenimports", set()))

    return hiddenimports


def register_default_launchers(loader,mode=None):
    """
    Register the default CommunityLaunchers into the given CommunityLoader.
    If you define a new default CommunityLauncher, add it here.
    """
    if mode == "master":
        loader.set_launcher(MasterCommunityLauncher())
    elif mode == "slave":
        loader.set_launcher(SlaveCommunityLauncher())
    elif mode == "relay":
        loader.set_launcher(RelayCommunityLauncher())
   
    
