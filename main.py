import asyncio
import argparse
from asyncio import get_event_loop, ensure_future
from session import Session


async def main():
    parser = argparse.ArgumentParser(description="Choose Mode of operation")
    parser.add_argument(
        "mode",
        help="Choose either master or slave mode",)

    args = parser.parse_args()
    if args.mode == None:
        print("Mode argument is required")
        exit()
    session = Session(mode=args.mode)
    await session.start(("127.0.0.1", 8097))

if __name__ == '__main__':
    ensure_future(main())
    get_event_loop().run_forever()
