import os
import sys
import asyncio
from asyncio import ensure_future, get_event_loop

import libnacl

from ipv8.community import Community, DEFAULT_MAX_PEERS
from ipv8.configuration import ConfigBuilder, Strategy, WalkerDefinition, default_bootstrap_defs
from ipv8_service import IPv8
from ipv8.lazy_community import lazy_wrapper
import ipv8_messages
import time
import uuid
import pickle
import grpc
import utils

sys.path.append("./service_spec")
import service_pb2
import service_pb2_grpc

registry_address = "195.201.197.25:4858"

class LinkEncryptedCommunity(Community):

    community_id = b'\xc2\x98da\x0b\xa7LN\x8a(\xc1\xfcV\xf7m0\xc5s\xc8\x21'

    def __init__(self, my_peer, endpoint, network, max_peers=DEFAULT_MAX_PEERS, anonymize=False):
        super().__init__(my_peer, endpoint, network, max_peers, anonymize)
        self.queue = []
        self.response_times = {}
        self.relays = {}
        self.chosen_relay = None
        self.session_created = False
        self.received_response = False
        self.once = False
        self.uid = uuid.uuid4()
        self.add_message_handler(ipv8_messages.PingReply, self.on_ping_reply)
        self.add_message_handler(ipv8_messages.QueryReply, self.on_query_reply)
        self.add_message_handler(ipv8_messages.SessionCreated, self.on_session_created)
        self.add_message_handler(ipv8_messages.DataPayload, self.on_datapayload)
        self.register_task("query", self.query, interval=15.0, delay=0)
        self.register_task("ping_relay", self.ping_relay, interval=5.0, delay=0)
        self.register_task("start_session", self.start_session)

    @lazy_wrapper(ipv8_messages.PingReply)   
    async def on_ping_reply(self, peer, payload):
        """[Receives the ping reply from the relay node and updates internal registry with response time]
        """ 
        # print("Received ping reply")
        if peer in self.relays.keys():
            received_time = time.time()
            response_time = received_time - self.relays[peer][0]
            self.relays[peer].append(response_time)
        else:
            self.relays[peer] = [time.time()]

    @lazy_wrapper(ipv8_messages.QueryReply)
    async def on_query_reply(self, peer, payload):
        # print("Query reply received")
        if not self.chosen_relay:
            self.chosen_relay = peer
            print("Chosen relay: ", self.chosen_relay._address)
        # Done until 2 

     
    @lazy_wrapper(ipv8_messages.SessionCreated)
    async def on_session_created(self, peer, payload):
        # print("Session created")
        self.session_created = True
        # Done until 7

    @lazy_wrapper(ipv8_messages.DataPayload)   
    async def on_datapayload(self, peer, payload):
        if not self.received_response:
            msg = pickle.loads(payload.content)
            master = msg['master']
            output = msg['output']
            if not master:
                print(output)
            self.received_response = True

    async def ping_relay(self):
        """[Constantly pings the list of discovered relay nodes ]
        """        
        if self.get_peers():
            for peer in self.get_peers():
                # self.relays[peer] = [time.time()]
                self.ez_send(peer,ipv8_messages.PingRequest(str.encode(str(time.time()))))

    async def start_session(self):
        endpoints = None
        if not self.relays.keys():
            print("No relay node discovered!!!")
            while not self.relays.keys():
                print("No relay node discovered!!!")
                await asyncio.sleep(0.5)
        try:
            endpoints = utils.get_endpoints("slave")
            # print("Endpoints: ", endpoints)
           
        except:
            print("Unable to find endpoints")
        
        if endpoints["peer_ids"]:
            peer_ids = endpoints["peer_ids"]
            ip_addrs = endpoints["endpoints"]
            peer_info = peer_ids[0]
            # for peer_info in peer_ids:
            #     key = LibNaCLPK(peer_info['key'])
            #     peer = Peer(key)
            #     peer.mid = peer_info['mid']
            #     peer.public_key = peer_info['public_key']
            #     peer._address = peer_info['_address']
            msg = {}
            msg['uid'] = self.uid
            msg['node'] = peer_info
            print(peer_info['mid'])
            for peer in self.relays.keys():
                self.ez_send(peer,ipv8_messages.QueryRelay(pickle.dumps(msg)))
                print("Sent to ", peer)
            
            while self.chosen_relay == None:
                print(self.chosen_relay)
                await asyncio.sleep(1/100)
                # for peer in self.relays.keys():
                #     self.ez_send(peer,ipv8_messages.QueryRelay(pickle.dumps(msg)))
                #     print(1)
            print(2)
            self.ez_send(self.chosen_relay,ipv8_messages.InitiateConnection(pickle.dumps(msg)))
            print(3)
            # Done until 3
            count = 0
            print("Sent initiate connection to ", self.chosen_relay._address)
            while not self.session_created:
                if count % 100 == 0:
                    self.ez_send(self.chosen_relay,ipv8_messages.InitiateConnection(pickle.dumps(msg)))
                count += 1
                await asyncio.sleep(1/100)
            
            print(">", end=" ")
            command = input()
            msg['command'] = command
            msg['master'] = True
            self.ez_send(self.chosen_relay, ipv8_messages.DataPayload(pickle.dumps(msg)))
            self.received_response = False

            self.register_task("run_shell", self.run_shell, interval=1.0,delay=0)
        
       


        
        


    async def run_shell(self):
        while not self.received_response:
            await asyncio.sleep(1/100)
        print(">", end=" ")
        command = input()
        if command == 'exit':
            self.cancel_pending_task(name="run_shell")
            asyncio.get_event_loop().stop()
            return
        msg = {}
        msg['uid'] = self.uid
        msg['command'] = command
        msg['master'] = True
        self.ez_send(self.chosen_relay, ipv8_messages.DataPayload(pickle.dumps(msg)))
        self.received_response = False




    async def query(self):
            
        reg = {
            'peer_id': '',
            'ip_addrs': '',
            'services': [],
        }
        
        ip_peer = None
        
        service_desc = {}
        service_meta = {}
        peer_info = {}
        addrs = []
        service_meta['name'] = "master"
        
        service_desc['name'] = "name"
        service_desc['endpoints'] = "addrs"
        service_meta['service_input'] = ''
        service_meta['service_output'] = ''
        service_meta['price'] = 0
        peer_info['nodeID'] = self.uid
        peer_info['key'] = self.my_peer.public_key.key_to_bin()
        peer_info['mid'] = self.my_peer.mid
        peer_info['public_key'] = self.my_peer.public_key.key_to_bin()
        peer_info['_address'] = self.my_peer._address
        ip_peer = self.my_peer._address
        peer_info['_address'] = ip_peer
        reg['peer_id'] = peer_info
        reg['ip_addrs'] = ["node_ip", "adapter_port"]
        reg['services'].append(service_meta)
                
        # Updates global orchestrator for automatic service deployment
        channel = grpc.insecure_channel(str(registry_address))
        stub = service_pb2_grpc.RegistryStub(channel)
        services = pickle.dumps(reg)
        try:
            if ip_peer != None and "UDPv4Address(ip='192" not in str(ip_peer) and "UDPv4Address(ip='127" not in str(ip_peer) and "UDPv4Address(ip='172" not in str(ip_peer) and "UDPv4Address(ip='10." not in str(ip_peer):
                response = stub.updateRegistry(
                    service_pb2.Services(services_info=services))
        except Exception as e:
            print(e)


        
            

        









































